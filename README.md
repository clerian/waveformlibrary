# waveformlibrary
a library for waveform analysis, including Fast Fourrier filters and pulse shape fitting with templates

the SMI waveform library is a system for plugin based waveform analysis for raw data in various formats,
currently inlcuded are ROOT files and MIDAS files if the data is stored in the raw data stream of a 
CAEM V1742 wavefrom digitiser
